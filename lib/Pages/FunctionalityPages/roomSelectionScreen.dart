import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../../Services/authentification.dart';
import '../../model/Devices.dart';
import '../ProfilePages/homeowner_profile.dart';
import '../flutter_flow/HomeAppTheme.dart';

class RoomSelectionScreen extends StatelessWidget {
  final List<Device> selectedDevices;
  final Authentication _authentication = Authentication();
  final FirebaseAuth _auth = FirebaseAuth.instance;
  late User currentUser;
  List<Object> _devicesList = [];

  RoomSelectionScreen({required this.selectedDevices});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: HomeAppTheme
          .of(context)
          .primaryBackground,
      appBar: PreferredSize(
        preferredSize: const Size.fromHeight(50),
        child: AppBar(
          title: Text(
            "Your selected devices",
            style: HomeAppTheme
                .of(context)
                .subtitle1,
          ),
          backgroundColor: HomeAppTheme
              .of(context)
              .primaryBackground,
          automaticallyImplyLeading: false,
          leading: IconButton(
            icon: const Icon(
              Icons.arrow_back,
              color: CupertinoColors.systemGrey,
            ),
            onPressed: () =>
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) =>
                        const HomeownerProfilePageWidget())),
          ),
          centerTitle: false,
          elevation: 0,
        ),
      ),
      body: Padding(
        padding: const EdgeInsets.all(11.0),
        child: ListView.builder(
          itemCount: selectedDevices.length,
          itemBuilder: (context, index) {
            final device = selectedDevices[index];

            return ListTile(
              leading: CircleAvatar(
                backgroundImage: NetworkImage(device.uploadedImage ?? ""),
              ),
              title: Text(device.deviceName!,
                style: HomeAppTheme
                    .of(context)
                    .subtitle2,),
            );
          },
        ),
      ),
    );
  }


  void getCurrentUser() async {
    try {
      final user = _auth.currentUser;
      if (user != null) {
        currentUser = user;
        print(currentUser.email);
      }
    } catch (e) {
      print(e);
    }
  }


  Future getUsersDeviceList() async {
    getCurrentUser();
    var uid = currentUser.uid;
    var data = await FirebaseFirestore.instance
        .collection("users")
        .doc(uid)
        .collection("devices")
        .orderBy('device name', descending: true)
        .get();

    if (data.size <= 0) {
      _devicesList = [];
    }
    else {
      _devicesList =
          List.from(data.docs.map((doc) => Device.fromSnapshot(doc)));


      print(_devicesList);
    }
  }

}
