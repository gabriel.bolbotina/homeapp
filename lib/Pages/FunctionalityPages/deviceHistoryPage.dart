import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_speed_dial/flutter_speed_dial.dart';

import '../../Services/Animations.dart';
import '../../Services/authentification.dart';
import '../../model/Devices.dart';
import '../../reusables/device_card.dart';
import '../ProfilePages/homeowner_profile.dart';
import '../flutter_flow/HomeAppTheme.dart';
import '../flutter_flow/flutter_flow_util.dart';
import 'addDevicePage.dart';


Stream<List<QueryDocumentSnapshot>> getDevices(String userId) {
  return FirebaseFirestore.instance
      .collection('users')
      .doc(userId)
      .collection('devices')
      .orderBy('timestamp', descending: true)  // Order by timestamp in descending order
      .snapshots()
      .map((snapshot) => snapshot.docs);
}



class DevicesHistoryPage extends StatefulWidget {

  const DevicesHistoryPage({Key? key}) : super(key: key);

  @override
  _DevicesHistoryPageState createState() =>
      _DevicesHistoryPageState();
}

class _DevicesHistoryPageState extends State<DevicesHistoryPage> {


  final scaffoldKey = GlobalKey<ScaffoldState>();
  bool isRefreshing = false;
  Device _device = Device();
  final Authentication _authentication = Authentication();
  final FirebaseAuth _auth = FirebaseAuth.instance;
  late User currentUser;
  List<Device> _devicesList = [];
  final FirebaseFirestore _firestore = FirebaseFirestore.instance;
  Device _devices = Device();
  DateTime now = DateTime.now();
  String? date;
  final bool _pinned = true;
  final bool _snap = false;
  bool _floating = false;
  bool isImageAvailable = false;
  late String? image;
  late String imageUrl;
  bool _hasDevices = false;

  @override
  Future<void> didChangeDependencies() async {
    // TODO: implement didChangeDependencies
    super.didChangeDependencies();
    getUsersDeviceList();
    fetchImage();
    await _authentication.getUserName();
    await _authentication.getProfileImage();

    print(isImageAvailable);
  }

  @override
  void initState() {
    fetchImage();
    getUsersDeviceList();
  }

  Future<void> fetchImage() async {
    imageUrl = (await _authentication.getProfileImage())!;
    print(imageUrl);
    setState(() {
      isImageAvailable = imageUrl != null && imageUrl.isNotEmpty;
    });
  }

  Future addUserDetails(String firstName, String lastName, int age) async {
    await FirebaseFirestore.instance.collection('users').add({
      'first name': firstName,
      'last name': lastName,
      'age': age,
    });
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: HomeAppTheme
          .of(context)
          .primaryBackground,
      appBar: PreferredSize(
        preferredSize: const Size.fromHeight(50),
        child: AppBar(
          title: Text(
            "Your selected devices",
            style: HomeAppTheme
                .of(context)
                .subtitle1,
          ),
          backgroundColor: HomeAppTheme
              .of(context)
              .primaryBackground,
          automaticallyImplyLeading: false,
          leading: IconButton(
            icon: const Icon(
              Icons.arrow_back,
              color: CupertinoColors.systemGrey,
            ),
            onPressed: () =>
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) =>
                        const HomeownerProfilePageWidget())),
          ),
          centerTitle: false,
          elevation: 0,
        ),
      ),
      body: Padding(
        padding: const EdgeInsets.all(11.0),
        child: ListView.builder(
          itemCount: _devicesList.length,
          itemBuilder: (context, index) {
            final device = _devicesList[index];

            return ListTile(
              leading: CircleAvatar(
                backgroundImage: NetworkImage( device.uploadedImage?? ""),
              ),
              title: Text(device.deviceName!,
                style: HomeAppTheme
                    .of(context)
                    .subtitle2,),
              subtitle: Text(convertTimestampToDayAdded(device.timestamp) ?? "No data found",
                style: HomeAppTheme
                    .of(context)
                    .subtitle2,),
            );
          },
        ),
      ),
    );
  }

  void _addDevice() {
    //_device.serialNumber =
  }

  void getCurrentUser() async {
    try {
      final user = _auth.currentUser;
      if (user != null) {
        currentUser = user;
        print(currentUser.email);
      }
    } catch (e) {
      print(e);
    }
  }


  Future getUsersDeviceList() async {
    getCurrentUser();
    var uid = currentUser.uid;
    var data = await FirebaseFirestore.instance
        .collection("users")
        .doc(uid)
        .collection("devices")
        .orderBy('device name', descending: true)
        .get();

    if (data.size < 0) {
      setState(() {
        // Update your state variable accordingly
        _devicesList = [];
        _hasDevices = false;
      });
    } else {
      setState(() {
        _devicesList =
            List.from(data.docs.map((doc) => Device.fromSnapshot(doc)));
        _hasDevices = true;
      });
    }
    print(_devicesList);
  }

  Future<void> refreshDevices() async {
    setState(() {
      isRefreshing = true;
    });

    await getUsersDeviceList();
    await fetchImage();

    setState(() {
      isRefreshing = false;
    });
  }

  bool isUrlValid(String url) {
    RegExp urlRegex = RegExp(
      r"^(http(s)?://)?([\w-]+\.)+[\w-]+(/[\w- ;,./?%&=]*)?$",
      caseSensitive: false,
      multiLine: false,
    );
    return urlRegex.hasMatch(url);
  }

  String convertTimestampToDayAdded(Timestamp? timestamp) {
    final dateTime = timestamp?.toDate();
    final now = DateTime.now();

    if(timestamp == null)
      return "Unknown";

    // Check if the timestamp is from today
    if (dateTime?.year == now.year && dateTime?.month == now.month && dateTime?.day == now.day) {
      return 'Today';
    }

    // Check if the timestamp is from yesterday
    final yesterday = DateTime(now.year, now.month, now.day - 1);
    if (dateTime?.year == yesterday.year && dateTime?.month == yesterday.month && dateTime?.day == yesterday.day) {
      return 'Yesterday';
    }

    // Return the formatted date if it's not today or yesterday
    final formatter = DateFormat('MMMM dd, yyyy');
    return formatter.format(dateTime!);
  }

}


