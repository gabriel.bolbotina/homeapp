
import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_hue/constants/api_fields.dart';
import 'package:flutter_hue/domain/models/bridge/bridge.dart';
import 'package:flutter_hue/domain/models/bridge/bridge_extensions.dart';
import 'package:flutter_hue/domain/models/hue_network.dart';
import 'package:flutter_hue/domain/models/light/light.dart';
import 'package:flutter_hue/domain/models/light/light_color/light_color_xy.dart';
import 'package:flutter_hue/domain/models/resource_type.dart';
import 'package:flutter_hue/domain/repos/bridge_discovery_repo.dart';
import 'package:flutter_hue/domain/repos/flutter_hue_maintenance_repo.dart';
import 'package:flutter_hue/domain/repos/token_repo.dart';
import 'package:homeapp/Pages/HomePages/homeowner.dart';
import 'package:homeapp/Pages/flutter_flow/homeAppWidgets.dart';
import 'package:uni_links/uni_links.dart';
import 'package:flutter_hue/flutter_hue.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../Services/Animations.dart';
import '../flutter_flow/HomeAppTheme.dart';

class PhilipsLight extends StatefulWidget {
  const PhilipsLight({Key? key}) : super(key: key);

  @override
  State<PhilipsLight> createState() => _PhilipsLightState();
}

class _PhilipsLightState extends State<PhilipsLight> {
  static const double padding = 15.0;
  late SharedPreferences sharedPreferences;
  bool isLoading = false;

  List<String> bridges = [];
  final List<String> bridgeIps = [];
  final DiscoveryTimeoutController timeoutController =
  DiscoveryTimeoutController(timeoutSeconds: 25);
  VoidCallback? onContactCancel;
  Bridge? bridge;
  HueNetwork? hueNetwork;
  Light? light;
  late final StreamSubscription deepLinkStream;

  @override
  void initState() {
    super.initState();

    deepLinkStream = uriLinkStream.listen(
          (Uri? uri) {
        if (uri == null) return;

        final int start = uri.toString().indexOf("?");
        String queryParams = uri.toString().substring(start);
        Uri truncatedUri = Uri.parse(queryParams);

        try {
          final String? pkce = truncatedUri.queryParameters[ApiFields.pkce];
          final String? code = truncatedUri.queryParameters[ApiFields.code];
          final String? resState =
          truncatedUri.queryParameters[ApiFields.state];

          // Handle Flutter Hue deep link
          if (pkce != null && code != null && resState != null) {
            String stateSecret;
            if (resState.contains("-")) {
              stateSecret = resState.substring(0, resState.indexOf("-"));
            } else {
              stateSecret = resState;
            }

            TokenRepo.fetchRemoteToken(
              clientId: "[clientId]",
              clientSecret: "[clientSecret]",
              pkce: pkce,
              code: code,
              stateSecret: stateSecret,
              decrypter: (ciphertext) =>
                  ciphertext.substring(4, ciphertext.length - 4),
            );
          }
        } catch (_) {
        }
      },
    );

    // Initialize Flutter Hue and keep all of the locally stored data up to
    // date.
    FlutterHueMaintenanceRepo.maintain(
      clientId: "[clientId]",
      clientSecret: "[clientSecret]",
      redirectUri: "flutterhue://auth",
      deviceName: "TestDevice",
      stateEncrypter: (plaintext) => "abcd${plaintext}1234",
    );

    SharedPreferences.getInstance().then((prefs) {
      setState(() {
        sharedPreferences = prefs;
      });
    });
  }

  @override
  void dispose() {
    deepLinkStream.cancel();

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: HomeAppTheme.of(context).primaryBackground,
      appBar: AppBar(
        backgroundColor:  HomeAppTheme.of(context).primaryBackground,
        automaticallyImplyLeading: false,
        leading: IconButton(
          icon: const Icon(
            Icons.arrow_back,
            color: CupertinoColors.systemGrey,
          ),
          onPressed: () => Navigator.pop(context),
        ),
        centerTitle: false,
        elevation: 0,
      ),
      body: SafeArea(
        child: SingleChildScrollView(
          child: Column(
            children: [
              const SizedBox(height: padding),

              sectionHeader("Getting Started"),

              // DISCOVER BRIDGES
              Column(
                children: [
                  HomeAppButtonWidget(
                    onPressed: discoverBridges,
                    text: "Discover Bridges", options: HomeAppButtonOptions(width: 130,
                    height: 40,
                    color: HomeAppTheme.of(context).primaryBtnText,
                    textStyle: HomeAppTheme.of(context).bodyText1,
                    elevation: 1,
                    borderSide: const BorderSide(
                      color: Colors.transparent,
                      width: 1,
                    ),
                    borderRadius: 20,),
                  ),
                  SizedBox(height: 10,),


                  Visibility(
                    visible: bridgeIps.isNotEmpty,
                    child: HomeAppButtonWidget(
                      onPressed: () => showIps(context),
                      text: "Found ${bridgeIps.length} bridge IP"
                          "${bridgeIps.length == 1 ? "" : "s"}",
                      options: HomeAppButtonOptions(width: 130,
                        height: 40,
                        color: HomeAppTheme.of(context).primaryBtnText,
                        textStyle: HomeAppTheme.of(context).bodyText1,
                        elevation: 1,
                        borderSide: const BorderSide(
                          color: Colors.transparent,
                          width: 1,
                        ),
                        borderRadius: 20,),

                    ),
                  ),
                ],
              ),

              const SizedBox(height: padding * 2),

              // FIRST CONTACT
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  ElevatedButton(
                    onPressed: bridgeIps.isEmpty ? null : () => firstContact(),
                    child: const Text("First Contact"),
                  ),
                  const SizedBox(width: 11),
                  ElevatedButton(
                    onPressed: onContactCancel,
                    child: const Text("Cancel"),
                  ),
                ],
              ),

              const SizedBox(height: padding * 2),

              ElevatedButton(
                onPressed: bridge == null ? null : remoteContact,
                child: const Text("Establish Remote Contact"),
              ),

              const SizedBox(height: padding * 2),

              sectionHeader("Reading Data"),

              ElevatedButton(
                onPressed: bridge == null ? null : fetchNetwork,
                child: const Text("Fetch Network"),
              ),

              const SizedBox(height: padding * 2),

              ElevatedButton(
                onPressed: bridge == null ? null : fetchBridge,
                child: const Text("Fetch Bridge"),
              ),

              const SizedBox(height: padding * 2),

              ElevatedButton(
                onPressed: bridge == null ? null : fetchLight,
                child: const Text("Fetch Light"),
              ),

              const SizedBox(height: padding * 2),

              sectionHeader("Writing Data"),

              ElevatedButton(
                onPressed: light == null ? null : identifyLight,
                child: const Text("Identify Light"),
              ),

              const SizedBox(height: padding * 2),

              ElevatedButton(
                onPressed: light == null ? null : toggleLight,
                child: const Text("Toggle Light on/off"),
              ),

              const SizedBox(height: padding * 2),


              const SizedBox(height: padding),
              HomeAppButtonWidget(
                onPressed: () =>
                { Navigator.push(
                    context,
                    Animations(
                      page: const HomeownerHomePageWidget(),
                      animationType: RouteAnimationType.slideFromBottom,
                    )),
                },
                text: "Skip ",
                options:HomeAppButtonOptions(width: 130,
                  height: 40,
                  color: HomeAppTheme.of(context).primaryBtnText,
                  textStyle: HomeAppTheme.of(context).bodyText1,
                  elevation: 1,
                  borderSide: const BorderSide(
                    color: Colors.transparent,
                    width: 1,
                  ),
                  borderRadius: 20,),

              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget sectionHeader(String title) {
    return Column(
      children: [
        Row(
          children: [
            const SizedBox(width: padding),
            Text(
              title,
              style: const TextStyle(
                fontSize: padding,
                fontWeight: FontWeight.bold,
              ),
            ),
          ],
        ),
        const Padding(
          padding: EdgeInsets.symmetric(horizontal: padding),
          child: Divider(thickness: 2.0),
        ),
      ],
    );
  }


  Future<void> firstContact() async {
    setState(() {
      onContactCancel = () => timeoutController.cancelDiscovery = true;
      isLoading = true;
    });

    //final String? bridgeIp = sharedPreferences.getString('bridgeIp');

    bridge = await BridgeDiscoveryRepo.firstContact(
      bridgeIpAddr: bridgeIps.first,
      controller: timeoutController,
    );

    print(bridge);

    sharedPreferences.setString('bridgeIp', bridgeIps.first.toString() ?? '');


    setState(() {
      onContactCancel = null;
      isLoading = false;
    });
  }


  void showIps(BuildContext context) {
    showDialog<void>(
      context: context,
      builder: (context) {
        return AlertDialog(
          title: const Text("Bridge IP"),
          content: SingleChildScrollView(
            child: ListBody(
              children: bridgeIps.map((ip) => Text(ip)).toList(),
            ),
          ),
          actions: [
            TextButton(
              child: const Text("Ok"),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  Future<void> discoverBridges() async {
    setState(() {
      isLoading = true;
    });

    bridges = await BridgeDiscoveryRepo.discoverBridges();

    setState(() {
      bridgeIps.addAll(bridges);
      isLoading = false;
    });
  }


  Future<void> fetchLight() async {
    setState(() {
      isLoading = true;
    });

    final Map<String, dynamic>? res =
        (await bridge!.getResource(ResourceType.light))?.first;

    // ignore: unused_local_variable
    Light light = Light.fromJson(res ?? {});

    setState(() {
      isLoading = false;
    });
  }


  /// Establishes remote contact with the bridge.
  Future<void> remoteContact() async {
    setState(() {
      isLoading = true;
    });

    await BridgeDiscoveryRepo.remoteAuthRequest(
      clientId: "[clientId]",
      redirectUri: "flutterhue://auth",
      deviceName: "TestDevice",
      encrypter: (plaintext) => "abcd${plaintext}1234",
    );

    setState(() {
      isLoading = false;
    });
  }

  Future<void> fetchNetwork() async {
    setState(() {
      isLoading = true;
    });
    hueNetwork = HueNetwork(bridges: [bridge!]);
    await hueNetwork?.fetchAll();
    try {
      light = hueNetwork!.lights.first;
    } catch (_) {
      // Do nothing
    }
    setState(() {
      isLoading = false;
    });
  }

  Future<void> fetchBridge() async {
    setState(() {
      isLoading = true;
    });

    final List<Map<String, dynamic>>? res =
    await bridge!.getResource(ResourceType.bridge);

    try {
      Bridge myBridge = Bridge.fromJson(res?.first ?? {});
    } catch (_) {
      // res list was empty
    }

    setState(() {
      isLoading = false;
    });
  }

  /// Causes the light to "breath" to let the user know which light they are
  /// working with.
  Future<void> identifyLight() async {
    setState(() {
      isLoading = true;
    });

    Device lightDevice;

    try {
      lightDevice = hueNetwork!.devices
      // ignore: deprecated_member_use
          .firstWhere((device) => device.metadata.name == light!.metadata.name);
    } catch (_) {
      return;
    }

    lightDevice.identifyAction = "identify";

    await bridge!.put(lightDevice);

    setState(() {
      isLoading = false;
    });
  }

  Future<void> toggleLight() async {
    setState(() {
      isLoading = true;
    });

    bool isOn = light!.isOn;

    light!.on.isOn = !isOn;

    await bridge!.put(light!);

    setState(() {
      isLoading = false;
    });
  }


}
