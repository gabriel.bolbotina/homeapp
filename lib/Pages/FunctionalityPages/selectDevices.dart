import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:homeapp/Pages/FunctionalityPages/roomSelectionScreen.dart';
import 'package:homeapp/Pages/flutter_flow/homeAppWidgets.dart';

import '../../Services/authentification.dart';
import '../../model/Devices.dart';
import '../ProfilePages/homeowner_profile.dart';
import '../flutter_flow/HomeAppTheme.dart';

// Step 2: Display the list of devices and allow device selection
class DeviceSelectionScreen extends StatefulWidget {
  @override
  _DeviceSelectionScreenState createState() => _DeviceSelectionScreenState();
}

class _DeviceSelectionScreenState extends State<DeviceSelectionScreen> {
  List<Device> _devices = []; // List to store devices
  List<Device> _selectedDevices = [];

  final Authentication _authentication = Authentication();
  final FirebaseAuth _auth = FirebaseAuth.instance;
  late User currentUser;
  bool isRefreshing = false;
  List<Object> _devicesList = [];// List to store selected devices

  @override
  void initState() {
    super.initState();
    _loadDevices();
  }

  void getCurrentUser() async {
    try {
      final user = _auth.currentUser;
      if (user != null) {
        currentUser = user;
        print(currentUser.email);
      }
    } catch (e) {
      print(e);
    }
  }

  Future<void> _loadDevices() async {
    try {
      getCurrentUser();
      var data = await FirebaseFirestore.instance
          .collection("users")
          .doc(currentUser.uid)
          .collection("devices")
          .orderBy('device name', descending: true)
          .get();

      if (data.size <= 0) {
        _devices = [];
      }
      else {
        _devices =
            List.from(data.docs.map((doc) => Device.fromSnapshot(doc)));
      }
    } catch (error) {
      // Handle error
    }
  }

  void _toggleDeviceSelection(Device device) {
    setState(() {
      if (_selectedDevices.contains(device)) {
        _selectedDevices.remove(device);
      } else {
        _selectedDevices.add(device);
      }
    });
  }


  Future<void> refreshDevices() async {
    setState(() {
      isRefreshing = true;
    });

    await _loadDevices();

    setState(() {
      isRefreshing = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: HomeAppTheme
          .of(context)
          .primaryBackground,
      appBar: PreferredSize(
        preferredSize: const Size.fromHeight(50),
        child: AppBar(
          title: Text(
            "Select devices to be added to this room",
            style: HomeAppTheme.of(context).subtitle1,
          ),
          backgroundColor: HomeAppTheme
              .of(context)
              .primaryBackground,
          automaticallyImplyLeading: false,
          leading: IconButton(
            icon: const Icon(
              Icons.arrow_back,
              color: CupertinoColors.systemGrey,
            ),
            onPressed: () =>
                Navigator.of(context).pop,
          ),
          centerTitle: false,
          elevation: 0,
        ),
      ),
      body: RefreshIndicator(
        onRefresh: refreshDevices,
        child: Padding(
          padding: const EdgeInsets.all(15.0),
          child: ListView.builder(
            itemCount: _devices.length,
            itemBuilder: (context, index) {
              final Device device = _devices[index];
              final isSelected = _selectedDevices.contains(device);

              return ListTile(
                title: Text(device.deviceName ?? "No device",

                style: HomeAppTheme.of(context).subtitle2
                ),
                leading: CircleAvatar(
                  backgroundImage: NetworkImage(device.uploadedImage ?? ""),
                ),
                trailing: Checkbox(
                  value: isSelected,
                  activeColor: HomeAppTheme.of(context).primaryColor,
                  onChanged: (_) => _toggleDeviceSelection(device),
                ),
              );
            },
          ),
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          // Proceed to step 4: Fetch the list of rooms or zones
          Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => RoomSelectionScreen(selectedDevices: _selectedDevices)),
          );
        },
        child: const Icon(Icons.arrow_forward,
        color: Colors.white,),
        //foregroundColor: HomeAppTheme.of(context).primaryColor,
        backgroundColor: HomeAppTheme.of(context).primaryColor,
      ),



    );
  }
}


