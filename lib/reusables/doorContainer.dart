import 'dart:convert';
import 'package:flutter/cupertino.dart';
import 'package:http/http.dart' as http;
import 'package:flutter/material.dart';
import '../Pages/flutter_flow/HomeAppTheme.dart';

class DoorStatusContainer extends StatefulWidget {
  @override
  _DoorStatusContainerState createState() => _DoorStatusContainerState();
}

class _DoorStatusContainerState extends State<DoorStatusContainer> {
  Future<int> fetchDoorStatus() async {
  var response = await http.get(Uri.parse(
      "https://api.thingspeak.com/channels/2191820/feeds.json?results=1"));

  if (response.statusCode == 200) {
    Map<String, dynamic> jsonResponse = jsonDecode(response.body) as Map<
        String,
        dynamic>;
    int fieldValue = int.parse(jsonResponse['feeds'][0]['field1']);
    return fieldValue;
    //var jsonResponse = jsonDecode(response.body);
    //var feeds = jsonResponse['feeds'];
  //if (fieldValue.isNotEmpty) { // Ensure feeds list is not empty
     // var lastFeed = feeds[feeds.length - 1];
      //var field1 = lastFeed['field1'];
      //return field1; // replace 'field1' with the name of your field in Thingspeak
   // else {
     // throw Exception('No feeds data available');
    //}
  } else {
    throw Exception('Failed to load data');
  }
}


@override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    return StreamBuilder<int>(
        stream: Stream.fromFuture(fetchDoorStatus()),
      builder: (context, snapshot) {
        if (snapshot.connectionState == ConnectionState.waiting) {
          return SizedBox(
              width: 20,
              height: 20,
              child: Container(
                width: size.width * 0.4,
                height: size.height * 0.13,
                decoration: BoxDecoration(
                    color: Colors.transparent,
                  borderRadius: BorderRadius.circular(20.0),
                ),
                child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,

                  children: [
                    CupertinoActivityIndicator(
                      color: HomeAppTheme.of(context).primaryColor,
                    ),
                  ],
                ),
              )); // Display a loading spinner while waiting for data
        } else if (snapshot.hasError) {
          return Text("Error: ${snapshot.error}");
        } else {
          bool isDoorOpen = snapshot.data == 11 ?? false; // Default to false if data is null

          return Container(
            width: size.width * 0.4,
            height: size.height * 0.13,
            decoration: BoxDecoration(
              color: isDoorOpen
                  ? HomeAppTheme.of(context).primaryColor.withOpacity(0.25)
                  : Colors.red,
              borderRadius: BorderRadius.circular(20.0),
            ),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Icon(
                  isDoorOpen ? Icons.check_circle : Icons.error,
                  size: 30.0,
                  color: Colors.black54,
                ),
                SizedBox(
                  height: 16.0,
                  width: 39,
                ),
                Text(
                  isDoorOpen ? 'Door is Open' : 'Door is Closed',
                  style: HomeAppTheme.of(context).subtitle2.override(
                    fontFamily: 'Poppins',
                    color: Colors.black54,
                  ),
                ),
              ],
            ),
          );
        }
      },
    );
  }
}
