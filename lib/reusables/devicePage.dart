import 'package:cached_network_image/cached_network_image.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../Pages/HomePages/homeowner.dart';
import '../Pages/flutter_flow/HomeAppTheme.dart';
import '../Pages/flutter_flow/homeAppWidgets.dart';
import '../model/Devices.dart';

class DeviceDetailPage extends StatelessWidget {
  //final Device device;
  final String deviceName;
  final String imageUrl;
  final int? serialNumber;
  final String? type;
  final String? brand;
  final Timestamp? timestamp;

  const DeviceDetailPage(
      {super.key, required this.deviceName,
      required this.imageUrl,
      this.serialNumber,
      this.type,
      this.brand,
      this.timestamp});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: CustomScrollView(
        slivers: [
          SliverAppBar(
            expandedHeight: 200,
            flexibleSpace: Stack(
              children: [
                Positioned.fill(
                  child: Hero(
                    tag: '$deviceName',
                    child: CachedNetworkImage(
                      imageUrl: imageUrl,
                      fit: BoxFit.cover,
                    ),
                  ),
                ),
                Positioned(
                  bottom: 16,
                  left: 16,
                  child: Text(
                    deviceName,
                    style: TextStyle(
                      fontSize: 24,
                      fontWeight: FontWeight.bold,
                      color: Colors.white,
                    ),
                  ),
                ),
              ],
            ),
          ),

          const SliverToBoxAdapter(
              child: SizedBox(
                height: 20,
              )),
          SliverToBoxAdapter(
              child: SizedBox(
                  height: 20,
                  child: Center(
                      child: Padding(
                        padding: const EdgeInsetsDirectional.fromSTEB(0, 0, 0, 30),
                        child: RichText(
                          overflow: TextOverflow.visible,
                          text: const TextSpan(
                            text: "Device info",
                            style: TextStyle(
                              fontSize: 15.0,
                              fontWeight: FontWeight.bold,
                              color: Colors.black38,
                            ),
                          ),
                        ),
                      )))),
          SliverToBoxAdapter(
            child: Padding(
                padding: EdgeInsetsDirectional.fromSTEB(24, 14, 24, 0),
                child: Container(
                    width: double.infinity,
                    height: 60,
                    decoration: BoxDecoration(
                      color: HomeAppTheme.of(context).secondaryBackground,
                      borderRadius: BorderRadius.circular(15),
                    ),
                    child: Center(
                      child: RichText(
                        text: TextSpan(
                          text: deviceName,
                          style: TextStyle(
                            fontSize: 15.0,
                            fontWeight: FontWeight.bold,
                            color: HomeAppTheme.of(context).secondaryText,
                          ),
                        ),
                      ),
                    ))),
          ),
      SliverToBoxAdapter(
        child: Padding(
              padding: EdgeInsetsDirectional.fromSTEB(24, 14, 24, 0),
              child: Container(
                  width: double.infinity,
                  height: 60,
                  decoration: BoxDecoration(
                    color: HomeAppTheme.of(context).secondaryBackground,
                    borderRadius: BorderRadius.circular(15),
                  ),
                  child: Center(
                    child: RichText(
                      text: TextSpan(
                        text: "$serialNumber",
                        style: TextStyle(
                          fontSize: 15.0,
                          fontWeight: FontWeight.bold,
                          color: HomeAppTheme.of(context).secondaryText,
                        ),
                      ),
                    ),
                  )))),
      SliverToBoxAdapter(
        child: Padding(
              padding: EdgeInsetsDirectional.fromSTEB(24, 14, 24, 0),
              child: Container(
                  width: double.infinity,
                  height: 60,
                  decoration: BoxDecoration(
                    color: HomeAppTheme.of(context).secondaryBackground,
                    borderRadius: BorderRadius.circular(15),
                  ),
                  child: Center(
                    child: RichText(
                      text: TextSpan(
                        text: brand ?? "",
                        style: TextStyle(
                          fontSize: 15.0,
                          fontWeight: FontWeight.bold,
                          color: HomeAppTheme.of(context).secondaryText,
                        ),
                      ),
                    ),
                  )))),

      SliverToBoxAdapter(
        child:Padding(
              padding: EdgeInsetsDirectional.fromSTEB(24, 14, 24, 0),
              child: Container(
                  width: MediaQuery.of(context).size.width * 0.4,
                  height: 60,
                  decoration: BoxDecoration(
                    color: HomeAppTheme.of(context).secondaryBackground,
                    borderRadius: BorderRadius.circular(15),
                  ),
                  child: Center(
                    child: RichText(
                      text: TextSpan(
                        text: type ?? "",
                        style: TextStyle(
                          fontSize: 15.0,
                          fontWeight: FontWeight.bold,
                          color: HomeAppTheme.of(context).secondaryText,
                        ),
                      ),
                    ),
                  )))),
          SliverToBoxAdapter(
            child: Padding(
              padding: const EdgeInsetsDirectional.fromSTEB(24, 14, 24, 0),
              child: Container(
                width: MediaQuery.of(context).size.width * 0.3,
                height: 60,
                child: HomeAppButtonWidget(
                  onPressed: () => {
                    if (deviceName != null)
                      {
                        //Navigator.of(context).pop,
                        showDialog(
                            context: context,
                            builder: (BuildContext context) {
                              return AlertDialog(
                                title: Text('Delete device'),
                                content:
                                const Text('Are you sure you want to delete the device?'),
                                actions: <Widget>[
                                  TextButton(
                                    child: Text('Cancel'),
                                    onPressed: () {
                                      Navigator.of(context).pop();
                                    },
                                  ),
                                  TextButton(
                                    child: Text('OK'),
                                    onPressed: () {
                                      deleteDevice(deviceName!);
                                      print("device deleted");
                                      Navigator.push(
                                          context,
                                          MaterialPageRoute(
                                              builder: (context) =>
                                              const HomeownerHomePageWidget()));
                                    },
                                  ),
                                ],
                              );
                            }
                        )
                      }
                  },

                  text: 'Delete',
                  options: HomeAppButtonOptions(
                    width: 110,
                    height: 50,
                    color: const Color.fromARGB(255, 255, 118, 118),
                    textStyle: HomeAppTheme.of(context).subtitle1.override(
                      fontFamily: 'Poppins',
                      color: Colors.black54,
                    ),
                    elevation: 3,
                    borderSide: const BorderSide(
                      color: Colors.transparent,
                      width: 1,
                    ),
                    borderRadius: 20,
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}


FirebaseAuth _auth = FirebaseAuth.instance;
Future<void> deleteDevice(String deviceName) async {
  try {
    final currentUser = _auth.currentUser;
    if (currentUser != null) {
      final uid = currentUser.uid;
      final deviceRef = FirebaseFirestore.instance
          .collection("users")
          .doc(uid)
          .collection("devices")
          .doc(deviceName);

      await deviceRef.delete();

      print('Device deleted: $deviceName');
    } else {
      print('User not logged in.');
    }
  } catch (e) {
    print('Error deleting device: $e');
  }
}

