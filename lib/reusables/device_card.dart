import 'dart:ui';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:homeapp/Pages/HomePages/homeowner.dart';

import '../Pages/ProfilePages/homeowner_profile.dart';
import '../Pages/flutter_flow/HomeAppTheme.dart';
import '../Pages/flutter_flow/homeAppWidgets.dart';
import '../model/Devices.dart';

import 'devicePage.dart';

class DeviceCard extends StatelessWidget {
  final Device _device;

  const DeviceCard(this._device, {super.key});

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return CupertinoContextMenu(
      actions: [
        Center(
          child: CupertinoContextMenuAction(
            child: Text('Edit'),
            trailingIcon: CupertinoIcons.sparkles,
          ),
        ),
        Center(
          child: CupertinoContextMenuAction(
            isDestructiveAction: true,
            child: Text('Delete'),
            trailingIcon: CupertinoIcons.delete,
            onPressed: () => {
              if (_device.deviceName != null)
                {
                  //Navigator.of(context).pop,
            showDialog(
            context: context,
            builder: (BuildContext context) {
              return AlertDialog(
                title: Text('Delete device'),
                content:
                const Text('Are you sure you want to delete the device?'),
                actions: <Widget>[
                  TextButton(
                    child: Text('Cancel'),
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                  ),
                  TextButton(
                    child: Text('OK'),
                    onPressed: () {
                      deleteDevice(_device.deviceName!);
                      print("device deleted");
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) =>
                              const HomeownerHomePageWidget()));
                    },
                  ),
                ],
              );
            }
            ),

                }
              else
                print("no device found with this name")
            },
          ),
        )
      ],
      previewBuilder:
          (BuildContext context, Animation<double> animation, Widget child) {
        return Container(
            child: ClipRRect(
                borderRadius: BorderRadius.circular(10),
                child: CachedNetworkImage(
                  imageUrl: _device.uploadedImage!,
                  width: MediaQuery.sizeOf(context).width * 0.75,
                  height: MediaQuery.sizeOf(context).height * 0.4,
                  fit: BoxFit.cover,
                  placeholder: (context, url) => SizedBox(
                      width: 20,
                      height: 20,
                      child: CupertinoActivityIndicator(
                        color: HomeAppTheme.of(context).primaryColor,
                      )), // Placeholder widget while loading
                  errorWidget: (context, url, error) => const Icon(
                      Icons.error), // Error widget when the image fails to load
                )));
      },
      child: Hero(
        tag: '$_device',
        child: GestureDetector(
          onTap: () => Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) => DeviceDetailPage(
                        deviceName: _device.deviceName!,
                        imageUrl: _device.uploadedImage ?? "",
                        serialNumber: _device.serialNumber!,
                        type: _device.type,
                        brand: _device.brand,
                        timestamp: _device.timestamp,
                      ))),
          child: Padding(
              padding: const EdgeInsetsDirectional.fromSTEB(16, 4, 16, 4),
              child: Container(
                //padding: EdgeInsetsDirectional.fromSTEB(0, 0, 10, 0),
                width: MediaQuery.of(context).size.width * 0.45,
                height: 190,
                decoration: BoxDecoration(
                  color:
                      HomeAppTheme.of(context).primaryColor.withOpacity(0.25),
                  // boxShadow: const [
                  //BoxShadow(
                  //blurRadius: 4,
                  //color: Color(0xFF8E97B9),
                  //offset: Offset(0, 2),
                  //)
                  //],
                  borderRadius: BorderRadius.circular(12),
                ),
                child: Padding(
                  padding: const EdgeInsetsDirectional.fromSTEB(0, 4, 4, 0),
                  child: Stack(
                      alignment: AlignmentDirectional.topStart,
                      textDirection: TextDirection.ltr,
                      children: [
                        ClipRRect(
                            borderRadius: BorderRadius.circular(10),
                            child: CachedNetworkImage(
                              imageUrl: _device.uploadedImage!,
                              width: double.infinity,
                              height: 115,
                              fit: BoxFit.cover,
                              placeholder: (context, url) => SizedBox(
                                  width: 20,
                                  height: 20,
                                  child: CupertinoActivityIndicator(
                                    color:
                                        HomeAppTheme.of(context).primaryColor,
                                  )), // Placeholder widget while loading
                              errorWidget: (context, url, error) => const Icon(Icons
                                  .error), // Error widget when the image fails to load
                            )),
                        BackdropFilter(
                            filter: ImageFilter.blur(sigmaX: 20, sigmaY: 30)),
                        Container(
                            alignment: Alignment.bottomLeft,
                            constraints: const BoxConstraints.tightFor(
                                width: 100, height: 400),
                            child: Padding(
                              padding: const EdgeInsetsDirectional.fromSTEB(
                                  8, 12, 0, 0),
                              child: DefaultTextStyle(
                                style: HomeAppTheme.of(context).subtitle2,
                                child: Text(
                                  _device.deviceName!,
                                ),
                              ),
                            )),
                        Container(
                          padding:
                              const EdgeInsetsDirectional.fromSTEB(8, 12, 0, 8),
                          alignment: Alignment.bottomRight,
                          child: Expanded(
                            child: HomeAppButtonWidget(
                              onPressed: () => Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => DeviceDetailPage(
                                            deviceName: _device.deviceName!,
                                            imageUrl:
                                                _device.uploadedImage ?? "",
                                            serialNumber: _device.serialNumber!,
                                            type: _device.type,
                                            brand: _device.brand,
                                            timestamp: _device.timestamp,
                                          ))),
                              text: 'Details',
                              options: HomeAppButtonOptions(
                                width: 60,
                                height: 40,
                                color: HomeAppTheme.of(context).primaryColor,
                                textStyle:
                                    HomeAppTheme.of(context).bodyText1.override(
                                          fontFamily: 'Poppins',
                                          color: HomeAppTheme.of(context)
                                              .secondaryText,
                                        ),
                                borderSide: const BorderSide(
                                  color: Colors.transparent,
                                  width: 1,
                                ),
                                borderRadius: 10,
                              ),
                            ),
                          ),
                        ),


                      ]),
                ),
              )),
        ),
      ),
    );
  }
}

FirebaseAuth _auth = FirebaseAuth.instance;
Future<void> deleteDevice(String deviceName) async {
  try {
    final currentUser = _auth.currentUser;
    if (currentUser != null) {
      final uid = currentUser.uid;
      final deviceRef = FirebaseFirestore.instance
          .collection("users")
          .doc(uid)
          .collection("devices")
          .doc(deviceName);

      await deviceRef.delete();

      print('Device deleted: $deviceName');
    } else {
      print('User not logged in.');
    }
  } catch (e) {
    print('Error deleting device: $e');
  }
}
