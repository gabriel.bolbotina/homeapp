import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../Pages/FunctionalityPages/roomSelectionScreen.dart';
import '../Pages/FunctionalityPages/selectDevices.dart';
import '../Pages/flutter_flow/HomeAppTheme.dart';
import '../Pages/flutter_flow/homeAppWidgets.dart';
import '../model/Devices.dart';

class RoomDetailsPage extends StatelessWidget {
  //final Device device;
  final String name;
  final Color color;
  final String imageUrl;

  RoomDetailsPage(
      {required this.name, required this.color, required this.imageUrl});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: CustomScrollView(
        slivers: [
          SliverAppBar(
            expandedHeight: 200,
            flexibleSpace: Hero(
              tag: 'roomName$name',
              child: Stack(
                children: [
                  Positioned.fill(
                    child: imageUrl.isEmpty

                ? Container(
                color: color,
              )

                    :CachedNetworkImage(
                        imageUrl: imageUrl,
                        fit: BoxFit.cover,
                      placeholder: (context, url) =>
                          SizedBox(
                              width: 20,
                              height: 20,
                              child: CupertinoActivityIndicator(
                                color: HomeAppTheme.of(context).primaryColor,
                              )),
                    )


                  ),
                 Positioned(
                    bottom: 16,
                    left: 16,
                    child: Text(
                      name,
                      style: TextStyle(
                        fontSize: 24,
                        fontWeight: FontWeight.bold,
                        color: Colors.white,
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
          /*SliverFillRemaining(
            child: Center(
              child: Text(
                'Device Details',
                style: TextStyle(fontSize: 24),
              ),
            ),
          ),*/
          SliverToBoxAdapter(
            child: Padding(
              padding: const EdgeInsetsDirectional.fromSTEB(0, 280, 0, 0),
              child: HomeAppButtonWidget(
                onPressed: () => Navigator.push(context,
                    MaterialPageRoute(builder: (context) =>  DeviceSelectionScreen())),

                text: 'Add devices',
                options: HomeAppButtonOptions(
                  width: 110,
                  height: 50,
                  color: const Color.fromARGB(255, 253, 238, 186),
                  textStyle: HomeAppTheme.of(context).subtitle1.override(
                    fontFamily: 'Poppins',
                    color: Colors.black54,
                  ),
                  elevation: 3,
                  borderSide: const BorderSide(
                    color: Colors.transparent,
                    width: 1,
                  ),
                  borderRadius: 20,
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
