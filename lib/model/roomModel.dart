import 'package:cached_network_image/cached_network_image.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:homeapp/reusables/roomPage.dart';
import 'dart:core';
import 'dart:ui';

import '../../Services/Animations.dart';
import '../Pages/HomePages/homeowner.dart';
import '../Pages/flutter_flow/HomeAppTheme.dart';
import 'Room.dart';

class RoomNamesWidget extends StatefulWidget {
  const RoomNamesWidget({Key? key}) : super(key: key);

  @override
  _RoomNamesWidgetState createState() => _RoomNamesWidgetState();
}

class _RoomNamesWidgetState extends State<RoomNamesWidget> {
  final FirebaseAuth _auth = FirebaseAuth.instance;
  late User currentUser;
  bool _hasRooms = false;
  List<Room> _roomList = [];

  @override
  void initState() {
    getUsersRoomList();
    _hasRooms = true;
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return  Builder(builder: (BuildContext context) {
              if (!_hasRooms) {
                return SizedBox(
                  height: 30,
                  child: Center(
                    child: Padding(
                      padding:
                          const EdgeInsetsDirectional.fromSTEB(0, 0, 0, 30),
                      child: RichText(
                        overflow: TextOverflow.visible,
                        text: const TextSpan(
                          text: "You have no rooms",
                          style: TextStyle(
                            fontSize: 18.0,
                            fontWeight: FontWeight.bold,
                            color: Colors.black38,
                          ),
                        ),
                      ),
                    ),
                  ),
                );
              } else  {
                return Container(
                    //height: size.height*0.2,
                    padding: EdgeInsets.all(16.0),
      color: HomeAppTheme.of(context).primaryBackground,
      child: SingleChildScrollView(
      scrollDirection: Axis.horizontal,
      child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: _roomList.map((Room room) {
                    return Row(
                      children: <Widget>[
                        RoomNameItem(room as Room),
                        SizedBox(width: 10),
                      ],
                    );
                  }).toList(),
                )));
              }

            },
            );
  }

  void getCurrentUser() async {
    try {
      final user = _auth.currentUser;
      if (user != null) {
        currentUser = user;
        print(currentUser.email);
      }
    } catch (e) {
      print(e);
    }
  }

  Future getUsersRoomList() async {
    getCurrentUser();
    var uid = currentUser.uid;
    var data = await FirebaseFirestore.instance
        .collection("users")
        .doc(uid)
        .collection("rooms")
        .orderBy('timestamp', descending: true)
        .get();

    if (data.size <= 0) {
      setState(() {
        // Update your state variable accordingly
        _roomList = [];
        _hasRooms = false;
        print(_hasRooms);
      });
    } else {
      setState(() {
        _roomList = List.from(data.docs.map((doc) => Room.fromSnapshot(doc)));
        _hasRooms = true;
        print(_hasRooms);
      });
    }
    print(_roomList);
  }
}

class RoomNameItem extends StatelessWidget {
  //final String name;

  //final String imageUrl;

  final Room _room;

  const RoomNameItem(this._room, {super.key});

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return CupertinoContextMenu(
      actions: [
        Center(
          child: CupertinoContextMenuAction(
            child: Text('Edit'),
            trailingIcon: CupertinoIcons.sparkles,
          ),
        ),
        Center(
          child: CupertinoContextMenuAction(
            isDestructiveAction: true,
            child: Text('Delete'),
            trailingIcon: CupertinoIcons.delete,
            onPressed: () => {
              if (_room.name != null)
                {
                  //Navigator.of(context).pop,
                  showDialog(
                      context: context,
                      builder: (BuildContext context) {
                        return AlertDialog(
                          title: Text('Delete room'),
                          content: const Text(
                              'Are you sure you want to delete this room?'),
                          actions: <Widget>[
                            TextButton(
                              child: Text('Cancel'),
                              onPressed: () {
                                Navigator.of(context).pop();
                              },
                            ),
                            TextButton(
                              child: Text('OK'),
                              onPressed: () {
                                deleteRoom(_room.name!);
                                print("device deleted");
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) =>
                                            const HomeownerHomePageWidget()));
                              },
                            ),
                          ],
                        );
                      }),
                }
              else
                print("no device found with this name")
            },
          ),
        )
      ],
      previewBuilder:
          (BuildContext context, Animation<double> animation, Widget child) {
        return Container(
          height: size.height * 0.1,
          width: size.width * 0.4,
          padding: EdgeInsets.symmetric(horizontal: 9.0, vertical: 4.0),
          decoration: BoxDecoration(
            color: HomeAppTheme.of(context).secondaryColor,
            //color: color.withOpacity(0.5),
            borderRadius: BorderRadius.circular(15.0),
            border:
                Border.all(color: Color(0xFF8E97B9).withOpacity(0.2), width: 2),
            //boxShadow:  [BoxShadow(
            //offset: Offset(0, 10),
            //blurRadius: 20,
            //color: Colors.black.withOpacity(0.23),
            //blurStyle: BlurStyle.normal,

            //)
            //  ],
          ),
          child: Align(
              alignment: AlignmentDirectional.center,
              child: DefaultTextStyle(
                style: HomeAppTheme.of(context).subtitle2.override(
                      fontFamily: 'Poppins',
                      color: Colors.black54,
                    ),
                child: Text(
                  _room.name ?? "",
                ),
              )),
        );
      },
      child: GestureDetector(
          onTap: () => Navigator.push(
                context,
                Animations(
                  page: RoomDetailsPage(
                    name: _room.name!,
                    color: HomeAppTheme.of(context)
                        .secondaryColor
                        .withOpacity(0.5),
                    imageUrl: _room.photoUrl!,
                  ),
                  animationType: RouteAnimationType.slideFromRight,
                ),
              ),
          child: Flexible(
              child: Container(
            height: size.height * 0.08,
            padding: EdgeInsets.symmetric(horizontal: 9.0, vertical: 4.0),
            decoration: BoxDecoration(
              color: HomeAppTheme.of(context).secondaryColor.withOpacity(0.5),
              //color: color.withOpacity(0.5),
              borderRadius: BorderRadius.circular(15.0),
              border: Border.all(
                  color: Color(0xFF8E97B9).withOpacity(0.2), width: 2),
              //boxShadow:  [BoxShadow(
              //offset: Offset(0, 10),
              //blurRadius: 20,
              //color: Colors.black.withOpacity(0.23),
              //blurStyle: BlurStyle.normal,

              //)
              //  ],
            ),
            child: Align(
                alignment: AlignmentDirectional.center,
                child: DefaultTextStyle(
                  style: HomeAppTheme.of(context).subtitle2.override(
                        fontFamily: 'Poppins',
                        color: Colors.black54,
                      ),
                  child: Text(
                    _room.name ?? "",
                  ),
                )),
          ))),
    );
  }
}

FirebaseAuth _auth = FirebaseAuth.instance;
Future<void> deleteRoom(String roomName) async {
  try {
    final currentUser = _auth.currentUser;
    if (currentUser != null) {
      final uid = currentUser.uid;
      final deviceRef = FirebaseFirestore.instance
          .collection("users")
          .doc(uid)
          .collection("rooms")
          .doc(roomName);

      await deviceRef.delete();

      print('Device deleted: $roomName');
    } else {
      print('User not logged in.');
    }
  } catch (e) {
    print('Error deleting device: $e');
  }
}
