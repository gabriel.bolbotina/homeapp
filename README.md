# Ghid de instalare pentru aplicația HomeApp

Bun venit la ghidul de instalare pentru aplicația mobilă HomeApp, o aplicație de automatizare inteligentă a casei. Această aplicație este compatibilă cu mai multe platforme și acest ghid vă va oferi pașii necesari pentru a rula aplicația pe dispozitive Android și iOS. Codul legat de modulul hardware poate fi găsit în folderul 'hardware code'.


## Cerințe preliminare

Înainte de a rula această aplicație, trebuie să aveți următoarele lucruri instalate:

1. Flutter SDK (versiunea 2.2.3 sau o versiune ulterioară) https://docs.flutter.dev/get-started/install 
2. Dart (versiunea 2.13.4 sau o versiune ulterioară) https://dart.dev
3. Android Studio sau IntelliJ sau VS Code cu plugin-ul Flutter instalat
4. Xcode (doar pentru iOS)
5. Git

Pentru a verifica dacă aveți Flutter și Dart instalate corect, rulați următoarele comenzi:

```bash
flutter --version
dart --version
```


## Pași pentru a rula aplicația

### 1. Clonați acest repository
Puteți utiliza următoarea comandă pentru a clona repository-ul:

```bash
git clone https://gitlab.upt.ro/gabriel.bolbotina/homeapp.git
```

### 2. Navigați în directorul repository-ului clonat:
```bash
cd homeapp
```

### 3.  Instalați pachetele Flutter și Dart.
Rulați următoarea comandă în rădăcina proiectului:
```bash
flutter pub get
```

## Android
1. Conectați dispozitivul Android sau porniți emulatorul Android.
2. Rulați următoarea comandă pentru a construi și rula aplicația:

```bash
flutter run
```

## iOS

1. Deschideți fișierul ios/Runner.xcworkspace în Xcode.
2. Configurați un echipă pentru proiectul dvs. Accesați Runner > Signing & Capabilities > Team și selectați echipa dvs.
3. Conectați dispozitivul iOS sau porniți simulatorul iOS.
4. Reveniți la terminal și rulați următoarea comandă:
```bash
flutter run
```

## Depanare
Dacă întâmpinați probleme, încercați următoarele:

1. Asigurați-vă că toate plugin-urile Flutter și Dart sunt actualizate.
2. Încercați să opriți aplicația și să o rulați din nou.
3. Dacă problema persistă, încercați o repornire completă prin oprirea aplicației, ștergerea ei de pe dispozitiv/emulator și rularea din nou.
