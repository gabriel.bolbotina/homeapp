#include <SoftwareSerial.h>
#include <Arduino.h>

SoftwareSerial espSerial(5, 6);
int doorState = 10;

int SoundSensor=0;  
int Lock=12; 
//reprezentare : 1-bataie, 0-pauza
int arr[6]={1,0,1,1,0,1}; 	
int j=0;
int state=0, lastState=0, pauza=0,start=0;
long start_clap, end_clap, begin_clapping;

void setup()  
{  
    pinMode(SoundSensor,INPUT);  
    pinMode(Lock,OUTPUT);   
    Serial.begin(9600);
    espSerial.begin(115200); //initialize serial  
} 
    
void loop()
{
    digitalWrite(Lock, HIGH);
    state=analogRead(0);	
    
    if(state>300)		//daca a auzit o bataie
    {
      Serial.println("clap");
      if(arr[j]==1)			//daca trebuia sa auda o bataie
      {
        end_clap=millis();
        if(start==0){			//daca bătaia este la inceput 
          begin_clapping=millis();		//tinem minte cand am inceput bataia
          //Serial.println(begin_clapping);
          start=1;
        }
        if(pauza==1){			//daca ar fi trebuit sa aibe loc o pauza
          if((end_clap-start_clap)<300){	//si aceasta nu a avut loc (pauza a fost prea scurta)
            Serial.println("too short");	
            espSerial.write(doorState);
            j=0;				//reinitializare
            start=0;
          }
        }
        start_clap=millis();			
        j++;			//avansam in array
        Serial.println(j);
        pauza=0;
      }
      else				//daca a auzit o bataie dar nu trebuia sa auda o bataie (arr[j]=0)
      {
        j=0;			//reinitializare
        start=0;
        pauza=0;
        espSerial.write(doorState);
        Serial.println("daca s-a auzit o bataie" + doorState);
      }
      delay(100);
    }

    else if(state<50)		//daca nu aude nimic
    {
      if(arr[j]==0)			//daca ne asteptam sa aibe loc o pauza
      {
        pauza=1;
        Serial.println("pauza");
        j++;			//avansam in array 
        Serial.println(j);
      }
 if(start==1 && pauza==0 && (millis()-start_clap)>300){	//daca au trecut mai mult de 300ms intre
//2 batai consecutive fara pauza
        Serial.println(0);
        j=0;							//reinitializare
        start=0;
        doorState =  10; 
        espSerial.write(doorState);	
        Serial.println("daca au trecut mai mutl de 10 secunde" + doorState);
  }
      
    }

    if(start==1 && (millis()-begin_clapping)>2000)		//daca au trecut mai mult de 2 secunde 
//de la inceperea bataii model
    {
      Serial.println(millis()-begin_clapping);
      Serial.println("2 seconds passed");
      j=0;								//reinitializare
      pauza=0;
      start=0;
      doorState = 10; 
      espSerial.write(doorState);	
      Serial.println(doorState);
    }
    
    if(j==6)						//daca am ajuns la sf in array
    {
      Serial.println("Done");
      digitalWrite(12,LOW);	
      doorState = 11; 
      espSerial.write(doorState);
      Serial.println(doorState);
      for(int k = 0; k <3; k++)
      {
        espSerial.write(11);
         Serial.println(doorState);		
          delay(20000);
          		
      }		//deblocam incuietoarea
      	//asteptam 3 secunde pentru ca persoana sa intre
      digitalWrite(12,HIGH);
      doorState = 10; 
      espSerial.write(doorState);	
      Serial.println(doorState);		//incuiem incuietoarea
      delay(300);
      j=0;						//reinitializare
      start=0;
      pauza=0;
    }

    //espSerial.write(doorState);
     //Serial.println(doorState);
}

