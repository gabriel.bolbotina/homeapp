#include <ESP8266WiFi.h>
#include <ThingSpeak.h>

const char* ssid = "ThisIsNotAHotspot";
const char* password = "demainseramieux";

unsigned long channelId = 2191820; 
const char *apiKey = "ZO4OXR8U4EIVT5M0"; 

WiFiClient client;

// variabilă pentru a salva starea ușii
int doorState = 10; 

void setup() {
  //setare comunicare serială
  Serial.begin(115200); 
  delay(10);

  // conectarea la WiFi
  Serial.println("\n");
  Serial.print("Connecting to WiFi: ");

  Serial.println(ssid);
  //conectarea propriu-zisă la rețea
  WiFi.begin(ssid, password);
  
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  //dacă modulul s-a conectat cu succes la WiFi se va afișa un mesaj corespunzător
  Serial.println("");
  Serial.println("WiFi connected");  
  
  // Se inițializează un client ThingSpeak
  ThingSpeak.begin(client);
}

void loop() {

   int upload = ThingSpeak.writeField(channelId, 1, 12, apiKey); 
   delay(20000); 
  // Dacă există date pe canalul de comunicare serială se va citi stadiul de blocare/ deblocare al ușii
  if (Serial.available() > 0) {

    while(Serial.available() > 0 ){
    // se citesc datele primite de la placa Arduino
    doorState = Serial.read(); 
    Serial.println(doorState);
    //se încarcă datele în Cloud, folosindu-ne de canalul creat
    int upload = ThingSpeak.writeField(channelId, 1, doorState, apiKey); 
    Serial.println(doorState);
    // în cazul încărcării reușite, se va afișa un mesaj pe monitorul serial
    if(upload == 200){
      Serial.println("Channel update successful.");
    }
    //în caz contrar se va afișa un mesaj de eroare
    else{
      //Serial.println("Problem updating channel. HTTP error code " + String(upload));
    }
    // ThingSpeak acceptă actualizări doar o dată la 15 secunde
    delay(20000); 
    }
  }
  
}
